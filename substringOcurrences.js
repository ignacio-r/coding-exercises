const assert = require("assert");

// Purpose: given two strings S0 and S1 determine how many ocurrences of S1 there are contiguously
// in S0.

S0 = "ababa";
S1 = "aba";

function substringOcurrences(S0, S1) {
  length = S1.length;
  stringArray = Array.from(S0);
  substrings = [];
  arrayLength = stringArray.length;
  while (arrayLength != 0) {
    substrings.push(stringArray.slice(0, length).join(""));
    stringArray.shift();
    arrayLength--;
  }
  console.log(substrings);
  return countOccurrences(substrings, S1);
}

const countOccurrences = (array, value) =>
  array.reduce((a, v) => (v === value ? a + 1 : a), 0);

assert.strictEqual(substringOcurrences(S0, S1), 2);
assert.strictEqual(substringOcurrences("helloworldhello", "hello"), 2);
assert.strictEqual(substringOcurrences("hellocathello", "hello"), 2);
assert.strictEqual(substringOcurrences("javahaskelljava", "haskell"), 1);
assert.strictEqual(substringOcurrences("batmanrobinbatrobin", "robin"), 2);
