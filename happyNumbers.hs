import Data.Char

isHappyNumber :: Int -> Bool
isHappyNumber x = happyNumber [] x

happyNumber :: [Int] -> Int -> Bool
happyNumber _ 1 = True 
happyNumber xs n = if elem sumOfSquares xs
                                        then False
                                        else happyNumber (sumOfSquares:xs) sumOfSquares
                                        where sumOfSquares = sum (squares (digits n))

digits :: Int -> [Int]
digits x = asIntDigits (show x)

asIntDigits :: String -> [Int]
asIntDigits cs = map digitToInt cs

squares :: [Int] -> [Int]
squares xs = map (\x->x*x) xs

happy :: [Int]
happy = [1, 7, 10, 13, 19, 23, 28, 31, 32, 44, 49, 68, 70, 79, 82, 86, 91, 94, 97, 100, 536, 556, 563, 565, 566, 608, 617, 622, 623, 632, 635, 637, 638, 644, 649, 653, 655, 656, 665, 671, 673, 680, 683, 694, 700, 709, 716, 736, 940, 946, 964, 970, 973, 989, 998, 1000]

unhappy :: [Int]
unhappy = [2, 3, 4, 5, 6, 8, 9, 11, 12, 14, 15, 16, 17, 18, 20, 21, 22, 24, 25, 26, 27, 29, 30, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 45, 46, 47, 48, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 69, 71, 72, 73, 74, 75, 76, 77, 78, 80, 81, 83]

areHappy :: [Int] -> [Bool]
areHappy xs = map isHappyNumber xs

testHappy = areHappy happy
testUnhappy = areHappy unhappy