const assert = require("assert");

// Purpose: Given a cache size and a list of elements for that cache, determine how many times the
// cache stays without mutations.
// Each element on the list represents one time for the cache.

cacheSize = 3;
list = [1, 2, 3, 2, 4, 2, 3, 3, 4, 2, 1, 2];
cacheSize2 = 2;
list2 = [9, 8, 7, 6, 7, 6, 6, 7, 7, 7];
cacheSize3 = 3;
list3 = [9, 8, 7];
cacheSize4 = 3;
list4 = [9, 8];

function timeWithoutChanges(cacheLen, list) {
  count = 0;
  counts = [];
  cache = new Set();
  listLen = list.length;
  i = 0;
  while ((cache.size !== cacheLen) & (listLen > 0)) {
    cache.add(list[i]);
    i++;
    listLen--;
  }
  if (list.length <= cacheLen) {
    return list.length;
  }
  cache = Array.from(cache);
  for (i = 0; i < list.length; i++) {
    if (!cache.includes(list[i])) {
      cache.shift();
      cache.push(list[i]);
      counts.push(count);
      count = 0;
    } else {
      count++;
      counts.push(count);
    }
  }
  console.log(cache);
  console.log("counts: " + counts);
  return counts.sort((a, b) => b - a)[0];
}

assert.strictEqual(timeWithoutChanges(cacheSize, list), 5);
assert.strictEqual(timeWithoutChanges(cacheSize2, list2), 6);
assert.strictEqual(timeWithoutChanges(cacheSize3, list3), 3);
assert.strictEqual(timeWithoutChanges(cacheSize4, list4), 2);
assert.strictEqual(timeWithoutChanges(3, [1, 1]), 2);
