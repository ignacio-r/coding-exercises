const assert = require("assert");

// Purpose: Given a number N determine the next smallest palindrome M > N.

function nextSmallestPalindrome(number) {
  if (areAllNines(number)) {
    return number + 2;
  }
  digits = String(number);
  left = getLeft(digits);
  right = getRight(digits);
  center = getCenter(digits);
  if (!reverseLeftHigherThanRight(left, right)) {
    leftCenter = increment(left, center);
    return buildNumber(leftCenter, center);
  }
  return mirrorNumber(left, center);
}

function increment(left, center) {
  return String(Number(left + center) + 1);
}

function mirrorNumber(left, center) {
  return Number(String(left + center + reverse(left)));
}

function buildNumber(leftCenter, center) {
  return Number(String(leftCenter + buildRight(leftCenter, center)));
}

function buildRight(leftCenter, center) {
  if (center) {
    return reverse(leftCenter.slice(0, -1));
  }
  return reverse(leftCenter);
}

function reverseLeftHigherThanRight(left, right) {
  return Number(reverse(left)) > Number(right);
}

function reverse(strNumber) {
  return Array.from(strNumber).reverse().join("");
}

function isOdd(number) {
  return number % 2;
}

function getLeft(strNumber) {
  const length = strNumber.length;
  if (isOdd(length)) {
    return strNumber.slice(0, Math.floor(length / 2));
  }
  return strNumber.slice(0, length / 2);
}

function getRight(strNumber) {
  const length = strNumber.length;
  if (isOdd(length)) {
    return strNumber.slice(-Math.floor(length / 2));
  }
  return strNumber.slice(-length / 2);
}

function getCenter(strNumber) {
  const length = strNumber.length;
  if (isOdd(length)) {
    return strNumber[Math.floor(length / 2)];
  }
  return "";
}

function areAllNines(number) {
  return [...new Set((number / 9).toString())]
    .map((each) => Number(each))
    .every((each) => each === 1);
}

function recursiveNSP(number) {
  number++;
  if (isPalindrome(number)) {
    return number;
  }
  return recursiveNSP(number);
}

function isPalindrome(element) {
  digits = element.toString();
  return digits === Array.from(digits).reverse().join("");
}

// Basic test cases
assert.strictEqual(nextSmallestPalindrome(0), 1);
assert.strictEqual(nextSmallestPalindrome(8), 9);
assert.strictEqual(nextSmallestPalindrome(9), 11);
assert.strictEqual(nextSmallestPalindrome(11), 22);
assert.strictEqual(nextSmallestPalindrome(15), 22);
assert.strictEqual(recursiveNSP(15), 22);

// Case1 - Odd and even length numbers - Reverse(Left) > Right
assert.strictEqual(nextSmallestPalindrome(1200), 1221);
assert.strictEqual(nextSmallestPalindrome(12300), 12321);
assert.strictEqual(recursiveNSP(12300), 12321);

// Case 2  - Odd and even length numbers - Reverse(Left) <= Right
assert.strictEqual(nextSmallestPalindrome(1221), 1331);
assert.strictEqual(nextSmallestPalindrome(12321), 12421);
assert.strictEqual(nextSmallestPalindrome(1224), 1331);
assert.strictEqual(nextSmallestPalindrome(12324), 12421);
assert.strictEqual(recursiveNSP(12324), 12421);

// Special test cases.
assert.strictEqual(nextSmallestPalindrome(9999), 10001);
assert.strictEqual(nextSmallestPalindrome(99999), 100001);
assert.strictEqual(recursiveNSP(99999), 100001);
